-- phpMyAdmin SQL Dump
-- version 4.1.13
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 產生時間： 2015 年 08 月 31 日 08:19
-- 伺服器版本: 5.1.36
-- PHP 版本： 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫： `read`
--

-- --------------------------------------------------------

--
-- 資料表結構 `announce`
--

CREATE TABLE IF NOT EXISTS `announce` (
  `ann_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `ann_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ann_content` text COLLATE utf8_unicode_ci NOT NULL,
  `ann_category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ann_author` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ann_posttime` datetime DEFAULT NULL,
  `ann_postview` smallint(25) DEFAULT NULL,
  PRIMARY KEY (`ann_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=434 ;

--
-- 資料表的匯出資料 `announce`
--

INSERT INTO `announce` (`ann_id`, `ann_title`, `ann_content`, `ann_category`, `ann_author`, `ann_posttime`, `ann_postview`) VALUES
(1, '測試公告1', '「天鵝」於今早上午8時轉為強颱，持續朝台灣東部海面移動。（圖翻攝自氣象局網站）. 「天鵝」減速變強颱最快今下午發海警. 颱風「天鵝」已', '未分類', 'admin', '2015-08-20 00:00:00', 5),
(3, '公告', '全球股災，歐美股市上周五主要指數全面暴跌作收，這股氣氛持續蔓延至今，拖累台股今日以7719.63點開出，下跌67.29點，在投資人信心匱乏下，相繼出現殺盤、恐慌性賣壓，盤中最低點來到7203.07點，狂瀉583.85點，跌幅7.49%，創下史上單日最大跌幅及跌點紀', '股災', '管理員', '2015-08-18 00:00:00', 5),
(4, '公告222', '全球股災，歐美股市上周五主要指數全面暴跌作收，這股氣氛持續蔓延至今，拖累台股今日以7719.63點開出，下跌67.29點，在投資人信心匱乏下，相繼出現殺盤、恐慌性賣壓，盤中最低點來到7203.07點，狂瀉583.85點，跌幅7.49%，創下史上單日最大跌幅及跌點紀', '股災', '管理員', '2015-08-18 00:00:00', 5),
(5, '公告333', '全球股災，歐美股市上周五主要指數全面暴跌作收，這股氣氛持續蔓延至今，拖累台股今日以7719.63點開出，下跌67.29點，在投資人信心匱乏下，相繼出現殺盤、恐慌性賣壓，盤中最低點來到7203.07點，狂瀉583.85點，跌幅7.49%，創下史上單日最大跌幅及跌點紀', '股災', '管理員', '2015-08-18 00:00:00', 5),
(427, '1', '2', NULL, '3', NULL, NULL),
(428, '2', '2', NULL, '3', NULL, NULL),
(429, '333', '333', NULL, '333', '0000-00-00 00:00:00', NULL),
(430, '555', '555', NULL, '555', '0000-00-00 00:00:00', NULL),
(433, '測試1', '<p><span style="font-size:48px"><span style="background-color:#FF0000">測試</span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#FFD700"><span style="font-size:10px"><span style="background-color:#FF0000">111</span></span></span></p>\r\n', NULL, '111', '2015-08-27 07:08:02', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `bookcategory`
--

CREATE TABLE IF NOT EXISTS `bookcategory` (
  `bo_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `bo_num` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bo_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bo_encategory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- 資料表的匯出資料 `bookcategory`
--

INSERT INTO `bookcategory` (`bo_id`, `bo_num`, `bo_category`, `bo_encategory`) VALUES
(1, '0001', '未分類', 'others');

-- --------------------------------------------------------

--
-- 資料表結構 `link`
--

CREATE TABLE IF NOT EXISTS `link` (
  `l_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `l_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`l_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- 資料表的匯出資料 `link`
--

INSERT INTO `link` (`l_id`, `l_name`, `l_link`) VALUES
(1, 'Google', 'http://www.google.com.tw'),
(3, '中信國小', '#'),
(4, 'YAHOO奇摩', '#'),
(5, '優學往', '#'),
(6, '生態農場', '#');

-- --------------------------------------------------------

--
-- 資料表結構 `rank`
--

CREATE TABLE IF NOT EXISTS `rank` (
  `ra_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `ra_booknum` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ra_bookname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ra_author` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ra_booksummary` text COLLATE utf8_unicode_ci,
  `ra_bookcategory` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ra_bookfrequency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ra_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- 資料表的匯出資料 `rank`
--

INSERT INTO `rank` (`ra_id`, `ra_booknum`, `ra_bookname`, `ra_author`, `ra_booksummary`, `ra_bookcategory`, `ra_bookfrequency`) VALUES
(1, '12345678', '網路尋寶記', '1', '南韓總統朴槿惠將於9月2日至4日訪問中國大陸，參與中國舉辦的抗戰勝利紀念活動。 據韓聯社消息，南韓總統朴槿惠將於9月2日至4日訪問中國大陸，參與中國舉辦', '未分類', '50'),
(2, '55555', '佐賀的超級阿罵', '2', '颱天鵝以每小時18轉23公里速度，向北北東轉東北進行。（圖翻攝自氣象局網站）. 天鵝北上甩風雨宜蘭、北部山區防大豪雨. 氣象局針對多處縣市發布豪雨、大雨特報，提醒民眾多加留意天氣訊息。（圖翻攝自氣象局網站）. （08:20更新內文，更新颱風路徑）中颱天鵝今晨 ...', '文學', '99'),
(3, '777777', '網頁百寶箱', '3', '亞洲貨幣21日受南北韓情勢緊張、中國經濟數字不如預期影響，幾乎貶成一片，新台幣終場也重貶1.98角，以32.876元兌1美元作收，即便該匯率已創下近6年半新低，台北市商業會理事長王應傑卻不假思索說：「還會貶。」且直言台幣貶勢的盡頭應落在34元左右。', '經濟', '80'),
(5, '67890', '雙城記222', '4', '日本首相安倍晉三下月初不會訪華，亦不會出席中國抗日戰爭勝利七十週年閱兵儀式。日本內閣官房長官菅義偉說，美國總統奧巴馬等多數歐美國家領袖，預計不會出席中方的閱兵儀式，安倍是希望保持步伐一致，亦考慮到參議院審議安保相關法案已到最', '外國新聞', '50'),
(6, '67890', '雙城記3333', '5', '日本首相安倍晉三下月初不會訪華，亦不會出席中國抗日戰爭勝利七十週年閱兵儀式。日本內閣官房長官菅義偉說，美國總統奧巴馬等多數歐美國家領袖，預計不會出席中方的閱兵儀式，安倍是希望保持步伐一致，亦考慮到參議院審議安保相關法案已到最', '外國新聞', '50'),
(7, '1234', '雙城記4', '6', '<p>日本首相安倍晉三下月初不會訪華，亦不會出席中國抗日戰爭勝利七十週年閱兵儀式。日本內閣官房長官菅義偉說，美國總統奧巴馬等多數歐美國家領袖，預計不會出席中方的閱兵儀式，安倍是希望保持步伐一致，亦考慮到參議院審議安保相關法案已到最</p>\r\n', '外國新聞', '501');

-- --------------------------------------------------------

--
-- 資料表結構 `reader`
--

CREATE TABLE IF NOT EXISTS `reader` (
  `re_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `div_id` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `re_name` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `re_summary` text COLLATE utf8_unicode_ci,
  `re_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `re_photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`re_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- 資料表的匯出資料 `reader`
--

INSERT INTO `reader` (`re_id`, `div_id`, `re_name`, `re_summary`, `re_link`, `re_photo`) VALUES
(1, 'ABCD', '王甲乙', '選舉將屆，新北市長兼國民黨主席朱立倫今天(20日)受訪時，針對國民黨總統參選人洪秀柱黨內民調上揚表示，現在是初期的階段，還是要持續努', 'http://www.google.com.tw', 'upload/photo/1440778834.jpg'),
(2, 'column2', '222', 'aaaadvda', 'http://localhost/', 'upload/photo/1440143896.jpg'),
(3, 'column3', '張無忌', '倚天屠龍記', 'https://esa.ntpc.edu.tw/', 'upload/photo/1440143916.jpg'),
(4, 'column4', '韋小寶', '鹿鼎記', 'https://translate.google.com.tw', 'upload/photo/1440143944.jpg');

-- --------------------------------------------------------

--
-- 資料表結構 `recommend`
--

CREATE TABLE IF NOT EXISTS `recommend` (
  `re_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `re_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `re_content` text COLLATE utf8_unicode_ci,
  `re_category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `re_author` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `re_posttime` datetime DEFAULT NULL,
  `re_postview` smallint(25) DEFAULT NULL,
  PRIMARY KEY (`re_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- 資料表的匯出資料 `recommend`
--

INSERT INTO `recommend` (`re_id`, `re_title`, `re_content`, `re_category`, `re_author`, `re_posttime`, `re_postview`) VALUES
(1, '推薦一本好書', '據凱基投顧分析師郭明錤（Ming-Chi Kuo）預測，蘋果12.9吋iPad Pro即將進入量產，時間可能介於9月或10月。（BusinessInsider）. 郭明錤表示iPad Pro將支援最新壓力感測功能', '未分類', '管理者', '2015-08-18 00:00:00', 23);

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `u_level` smallint(5) DEFAULT NULL,
  `u_leveltitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_account` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `u_password` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- 資料表的匯出資料 `user`
--

INSERT INTO `user` (`u_id`, `u_level`, `u_leveltitle`, `u_name`, `u_account`, `u_password`) VALUES
(1, 99, '管理者', '黃勝傑', 'admin', '1234');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

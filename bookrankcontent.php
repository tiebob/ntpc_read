<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>中信愛閱讀～閱讀推動網站</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="http://fonts.googleapis.com/css?family=Chivo:400,900" rel="stylesheet" />
    <link href="default.css" rel="stylesheet" type="text/css" media="all" />
    <link href="fonts.css" rel="stylesheet" type="text/css" media="all" />
    <!--[if IE 6]>
    <link href="default_ie6.css" rel="stylesheet" type="text/css" />
    <![endif]-->
</head>
<body>
<div id="wrapper">
    <div id="header-wrapper">
<!--        <div id="header" class="container">-->
<!--            <div id="logo">-->
<!--                <h1><a href="#">中信愛閱讀</a></h1>-->
<!--                <p>大家一起來閱讀</p>-->
<!--            </div>-->
<!--        </div>-->
    </div>
    <div id="banner"></div>
    <div id="menu-wrapper">
    <?php require_once ('menu.php'); ?>
        <!-- end #menu -->
    </div>
    <div id="page" class="container">
        <?php include_once ('content.php'); ?>
        <?php include_once ('sidebar.php'); ?>
    </div>
</div>
<div id="portfolio-wrapper">
        <?php include_once ('reader.php'); ?>
</div>

<div style="clear:both;padding-left: 50px;" id="footer-wrapper">
    <div id="footer" class="container">
        <?php include_once ('announce.php'); ?>
        <?php include_once ('link.php'); ?>
        <?php include_once ('contact.php'); ?>
</div>
</div>
</div>
        <?php include_once ('footer.php'); ?>
</body>
</html>
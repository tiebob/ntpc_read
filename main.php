<?php
$file=$_GET[file];
include_once ('function.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>好書推薦</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="http://fonts.googleapis.com/css?family=Chivo:400,900" rel="stylesheet" />
    <link href="default.css" rel="stylesheet" type="text/css" media="all" />
    <link href="fonts.css" rel="stylesheet" type="text/css" media="all" />
    <!--[if IE 6]>
    <link href="default_ie6.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script src="ckeditor/ckeditor.js"></script>
</head>
<body>
<div id="wrapper">
    <div id="header-wrapper">
    </div>
    <div id="banner"></div>
    <div id="menu-wrapper">
        <?php require_once ('menu.php'); ?>
        <!-- end #menu -->
    </div>
    <div id="page" class="container">
        <?php
        if ($file == "announce"){
            include('announcelist.php');
        }elseif ($file == "addannounce"){
            include('addannounce.php');
        }elseif ($file == "announcecontent"){
            include('announcecontent.php');
        }elseif ($file == "editannounce"){
            include('editannounce.php');
        }elseif ($file == "editbook"){
            include('editbook.php');
        }elseif ($file == "editreader"){
            include('editreader.php');
        }elseif ($file == "goodbook"){
            include('booklist.php');
        }elseif ($file == "readerlist"){
            include('readerlist.php');
        }else{
            echo "你點錯了";
        }
        ?>
    </div>
</div>

<?php include_once ('footer.php'); ?>
</body>
</html>
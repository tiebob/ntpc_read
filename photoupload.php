<?php
require_once ('conn.php');
$target_path = 'upload/photo/'.time().$_FILES['re_photo']['name'];
// 透過php的move_uploaded_file這個函式，將剛剛上傳到伺服器暫存資料夾的檔案正式移動到你所設定的網站目錄底下
if(move_uploaded_file($_FILES['re_photo']['tmp_name'], $target_path)) {
    echo "檔案 ".$_FILES['re_photo']['name']." 上傳成功.";
} else {
    echo "上傳失敗";
    echo "檔案名稱: ". $_FILES['re_photo']['name'];
    echo "檔案路徑: ".$target_path;
}
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="班級網頁登入管理">
    <meta name="author" content="黃勝傑">
    <title>班級網頁登入管理
    </title>
    <link rel="shortcut icon" href="favicon.ico">
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-primary text-center"><strong>班級網頁登入管理</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="text-center" style="margin-bottom: 10px;">
                        <img class="img-circle" src="images/loginphoto.png?sz=120" alt="">
                    </div>
                    <form role="form" name="form" method="post" action="loginconnect.php">
                        <fieldset>
                            <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                          <i class="glyphicon glyphicon-user"></i>
                                        </span>
                                        <input class="form-control" placeholder="Username" name="loginname" type="text" autofocus>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                          <i class="glyphicon glyphicon-lock"></i>
                                        </span>
                                        <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                    </div>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">記住帳號
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-primary">登入</button>

                                <a href="login-openid.php" style="text-decoration: none"><img src="images/ntpc-openid.png"></a>

                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<footer>
    <nav class="navbar navbar-inverse navbar-default bg-primary">
        <div class="container" style="margin-top: 15px;">
            <div class="text-center">
                <strong>程式開發： 黃勝傑</strong>
            </div>
        </div>
    </nav>
</footer>
</body>
</html>